package com.example.tapgame;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by piotr on 17.05.15.
 */
public class Game_rules extends Activity {

    protected TextView Rules;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rules);
        Rules = (TextView) findViewById(R.id.rules_content);
        Rules.setText("Hit the button how many times you can in only 10 seconds. New features are coming!\n\n" + "Written by Piotr Ekert");

    }
}
