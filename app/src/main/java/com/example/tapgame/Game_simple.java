package com.example.tapgame;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by piotr on 14.05.15.
 */

public class Game_simple extends ActionBarActivity {

    protected int COUNTER = 0;
    protected TextView score;
    protected TextView timer;
    protected Button tapButton;

    public SharedPreferences preferenceSettings;
    public SharedPreferences.Editor preferenceEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple);

        score = (TextView) findViewById(R.id.textView1);
        timer = (TextView) findViewById(R.id.textView2);
        tapButton = (Button) findViewById(R.id.button1);
        score.setText("Your actual score: " + COUNTER);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("Seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                timer.setText("Done!");
                if (timer.getText() == "Done!") {
                    tapButton.setClickable(false);
                    tapButton.setText("GAME OVER!");
                    showFinalResult(score);
                    preferenceSettings = getSharedPreferences("MyPref", 0);
                    preferenceEditor = preferenceSettings.edit();
                    preferenceEditor.putInt("score1", COUNTER);
                    preferenceEditor.apply();
                }
            }
        }.start();
    }

    public void showFinalResult(View view) {
        Toast.makeText(this, "Your final result is: " + COUNTER, Toast.LENGTH_LONG).show();
    }

    public void tapping(View v) {
        COUNTER++;
        Count(score);
    }

    public void Count(View v) {
        score.setText("Twój wynik: " + COUNTER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent others = new Intent(this, Game_settings.class);
            startActivity(others);
        }
        return super.onOptionsItemSelected(item);
    }
}
