package com.example.tapgame;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by piotr on 17.05.15.
 */
public class Game_scores extends Activity {

    protected TextView Points;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scores);

        SharedPreferences preferenceSettings = getSharedPreferences("MyPref", 0);

        Points = (TextView) findViewById(R.id.scores_content);
        long bestScore = preferenceSettings.getInt("score1", 0);
        Points.setText("Last score: " + bestScore);
    }
}
