package com.example.tapgame;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

    public void click_on_1(View v)
    {
        Intent k = new Intent (this, Game_simple.class);
        startActivity(k);
    }

    public void click_on_2(View v)
    {
        Intent k = new Intent (this, Game_numerical.class);
        startActivity(k);
    }

	public void click_on_3(View v)
	{
		Intent i = new Intent(this, Game_rules.class);
		startActivity(i);
	}

    public void click_on_4(View v)
    {
        Intent i = new Intent(this, Game_scores.class);
        startActivity(i);
    }

	public void goodbye(View v)
	{
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent others = new Intent(this, Game_settings.class);
            startActivity(others);
        }
		return super.onOptionsItemSelected(item);
	}

    //TODO: Rearange the code.
    //TODO: Make it more object-oriented.
	//TODO: make numerical moder playable.
}
